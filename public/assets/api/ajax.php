<?php

$action = $_POST['action'];

// Routes

if ($action === 'order') {
    $order = new Order();
    $order->register($_POST['order']);
    Ajax::sendResponse([ 'status' => 'success' ]);
}

if ($action === 'add-user') {
    $users = new UserActions($_POST['username'], $_POST['password']);
    $hasRegistered = $users->registerUser();

    if ($hasRegistered) {
        Ajax::sendResponse([ 'status' => 'success' ]);
    } else {
        Ajax::sendResponse([ 'status' => 'failed' ]);
    }

    die();
}

if ($action === 'login') {
    $users = new UserActions($_POST['username'], $_POST['password']);
    $hasLoggedIn = $users->loginUser();

    if ($hasLoggedIn) {
        Ajax::sendResponse([ 'status' => 'success' ]);
    } else {
        Ajax::sendResponse([ 'status' => 'failed' ]);
    }
}

class Order {
    private $database;

    public function __construct() {
        $this->database = new Database();
    }

    public function register(Array $data): bool {
        $this->database->insert('orders', $data);
        return true;
    }
}

class Database {
    private $connection;

    public function __construct() {
        $this->connection = new mysqli('database', 'lamp', 'lamp', 'lamp');

        if ($this->connection->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->connection->connect_error;

            exit();
        }
    }

    public function insert(String $table, Array $data): void {
        $rowCount = count($data);
        $loopCounter = 0;
        $columns = '';
        $values = '';

        foreach ($data as $key => $value) {
            if ($loopCounter === $rowCount - 1) {
                $columns .= sprintf('%s', $key);
                $values .= sprintf('"%s"', $value);
            } else {
                $columns .= sprintf('%s, ', $key);
                $values .= sprintf('"%s", ', $value);
                $loopCounter += 1;
            }
        }

        $sql = sprintf('INSERT INTO %s (%s) VALUES (%s)', $table, $columns, $values);
        mysqli_query($this->connection, $sql);
    }

    public function get(String $table, String $conditional) {
        $sql = sprintf('SELECT * FROM %s where %s', $table, $conditional);

        $result = mysqli_query($this->connection, $sql);

        return $result->fetch_assoc();
    }

    public function count(String $table, String $conditional) {
        $sql = sprintf('SELECT count(*) as count FROM %s where %s', $table, $conditional);

        $result = mysqli_query($this->connection, $sql);
        $count = $result->fetch_assoc()['count'];

        return intval($count);
    }
}

class UserActions {
    private $username;
    private $password;
    private $status;
    private $database;

    public function __construct(String $username, String $password) {
        $this->username = $username;
        $this->password = md5($password);
        $this->database = new Database();
    }

    public function getStatus(): Bool {
        return $this->status;
    }

    private function checkIfUserExists(): Bool {
        $count = $this->database->count('users', sprintf('username="%s"', $this->username));

        if ($count === 0) {
            return false;
        }

        return true;
    }

    private function verifyPassword(): Bool {
        $conditional = sprintf('username="%s" and password="%s"', $this->username, $this->password);
        $count = $this->database->count('users', $conditional);

        if ($count === 0) {
            return false;
        }

        return true;
    }

    public function loginUser(): Bool {
        if ($this->verifyPassword()) {
            return true;
        }

        return false;
    }

    public function registerUser(): Bool {
        if (!$this->checkIfUserExists()) {
            $this->database->insert('users', [
                'username' => $this->username,
                'password' => $this->password,
            ]);

            return true;
        }

        return false;
    }
}


class Ajax {
    public static function sendResponse(Array $data): void {
        echo json_encode($data);
    }
}
