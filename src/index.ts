import $ from 'jquery';
import './index.scss';

interface ILoginResponse {
    status: string;
}

interface IOrderClassic {
    username: string;
    size: string;
    selection: string;
    milk: string;
}

interface IOrderCustom {
    username: string;
    size: string;
    selection: string;
    milk: string;
    additives: string;
    syrup: string;
    foam: string;
    balance: string;
}

class Ajax {
    static path = `${document.URL}assets/api/ajax.php`;
}

class Pay {
    private element: JQuery = $('.pay');
    private ajax: JQuery.jqXHR;

    constructor(ajax: JQuery.jqXHR) {
        this.ajax = ajax;
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.element.addClass('is-hidden');
                $('.order-status').removeClass('is-hidden');
            } else {
                alert('Something went wrong :/, We are working on it');
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to register, please try again later :(');
        });
    }
}

class Final {
    private element: JQuery = $('.final');
    private summaryContainer: JQuery;
    private orderType: string;
    private orderClassic: IOrderClassic;
    private orderCustom: IOrderCustom;
    private ajax: JQuery.jqXHR;

    constructor() {
        this.orderType = $('[data-type]').data('type');
        this.summaryContainer = this.element.find(`.final__details`);
        this.init();
    }

    removeClasses(element: JQuery): JQuery {
        return element.removeClass().addClass('final__item');
    }

    init() {
        this.element.removeClass('is-hidden');
        this.element.find(`.final__button`).on('click.final', this.sendOrder.bind(this));
        this.buildSummary();
    }

    sendOrder(): void {
        this.element.addClass('is-hidden');

        if (this.ajax) {
            this.ajax.abort();
        }

        const payload: IOrderClassic | IOrderCustom = this.orderType === 'custom' ? this.orderCustom : this.orderClassic;

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'order',
                order: payload
            }
        });

        new Pay(this.ajax);
    }

    buildSummary(): void {
        if (this.orderType === 'custom') {
            this.buildCustomSummary();

            return;
        }

        this.buildClassicSummary();
    }

    buildCustomSummary(): void {
        const size: string = $(`.coffee-size`).data('size');
        const selection: string = $(`.coffee-type`).data('shot');
        const milk: string = $(`.milk-selection`).data('milk');
        const syrup: string = $(`.syrup`).data('syrup');
        const additives: string = $(`.additives`).data('additives');
        const balance: string = $(`[data-milkbalance]`).data('milkbalance');
        const foam: string = $(`[data-milkfoam]`).data('milkfoam');

        this.summaryContainer.append('<span class="is-option">size: ' + size + '</span><br>');
        this.summaryContainer.append('<span class="is-option">you asked for ' + selection + ' of espresso <br>');
        this.summaryContainer.append('<span class="is-option">milk type: ' + milk + '<br>');
        this.summaryContainer.append('<span class="is-option">syrup: ' + syrup + '<br>');
        this.summaryContainer.append('<span class="is-option">additives: ' + additives + '<br>');
        this.summaryContainer.append('<span class="is-option">balance: ' + balance + '<br>');
        this.summaryContainer.append('<span class="is-option">foam: ' + foam + '<br>');

        // Build data for AJAX
        this.orderCustom = {
            username: Login.username,
            size: size,
            milk: milk,
            selection: selection,
            additives: additives,
            syrup: syrup,
            balance: balance,
            foam: foam,
        }
    }

    buildClassicSummary(): void {
        const size: string = $(`.coffee-size`).data('size');
        const selection: string = $(`.coffee-type`).data('preparation');
        const milk: string = $(`.milk-selection`).data('milk');

        this.summaryContainer.append(size + '<br>');
        this.summaryContainer.append(selection + '<br>');
        this.summaryContainer.append(milk);

        // Build data for ajax
        this.orderClassic = {
            username: Login.username,
            size: size,
            milk: milk,
            selection: selection
        }
    }
}

class Additives {
    private element: JQuery = $('.additives');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.additives__option`).on('click.additives', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        this.element.get(0).dataset.additives = clickedElement.data('additives');

        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();
        new Final();
    }

    destroy(): void {
        this.element.find(`.additives__option`).off('click.additives');
    }
}

class Syrup {
    private element: JQuery = $('.syrup');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.syrup__option`).on('click.syrup', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        this.element.get(0).dataset.syrup = clickedElement.data('syrup');

        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();
        new Additives();
    }

    destroy(): void {
        this.element.find(`.syrup__option`).off('click.syrup');
    }
}

class MilkFoam {
    private element: JQuery = $('.milk-foam');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-foam__slider`).on('input.milk-foam', this.updatePercentage.bind(this));
        this.element.find(`.milk-foam__next`).on('click.milk-foam', this.triggerNavigation.bind(this));
    }

    updatePercentage(event: JQuery.TriggeredEvent): void {
        const balanceValue: number = Number($(event.currentTarget).val());

        this.element.find(`.milk-foam__percentage`).text(`${balanceValue} %`);
        this.element.get(0).dataset.milkfoam = String(balanceValue);
    }

    triggerNavigation(event: JQuery.ClickEvent): void {
        event.preventDefault();

        this.element.addClass('is-hidden');
        this.destroy();
        new Syrup();
    }

    destroy(): void {
        this.element.find(`.milk-foam__next`).off('click.milk-foam');
        this.element.find(`.milk-foam__slider`).off('change.milk-foam');
    }
}

class MilkBalance {
    private element: JQuery = $('.milk-balance');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-balance__slider`).on('input.milk-balance', this.updatePercentage.bind(this));
        this.element.find(`.milk-balance__next`).on('click.milk-balance', this.triggerNavigation.bind(this));
    }

    updatePercentage(event: JQuery.TriggeredEvent): void {
        const balanceValue: number = Number($(event.currentTarget).val());

        this.element.find(`.milk-balance__percentage`).text(`${balanceValue} %`);
        this.element.get(0).dataset.milkbalance = String(balanceValue);
    }

    triggerNavigation(event: JQuery.ClickEvent): void {
        event.preventDefault();

        this.element.addClass('is-hidden');
        this.destroy();
        new MilkFoam();
    }

    destroy(): void {
        this.element.find(`.milk-balance__next`).off('click.milk-balance');
        this.element.find(`.milk-balance__slider`).off('change.milk-balance');
    }
}

class MilkSelection {
    private element: JQuery = $('.milk-selection');

    private orderType: string;

    constructor() {
        this.orderType = $('[data-type]').data('type');
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.milk-selection__option`).on('click.milk-selection', this.handleSelection.bind(this));
    }

    handleSelection(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        this.element.get(0).dataset.milk = clickedElement.data('milk');

        this.triggerNavigation();
    }

    triggerNavigation(): void {
        this.element.addClass('is-hidden');
        this.destroy();

        if (this.orderType === 'custom') {
            new MilkBalance();

            return;
        }

        //if classic order type -> to final
        new Final();
    }

    destroy(): void {
        this.element.find(`.milk-selection__option`).off('click.milk-selection');
    }
}

class CoffeeType {
    private element: JQuery = $('.coffee-type');

    private orderType: string;

    constructor() {
        this.orderType = $('[data-type]').data('type');

        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.showSelection();
        this.addClickEvents();
    }

    addClickEvents(): void {
        this.element.find(`.coffee-type__list-item`).on('click.coffee-type', this.selectHandler.bind(this));
    }

    selectHandler(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        const orderType = $('[data-type]').data('type');

        if(orderType==='custom'){
            this.element.get(0).dataset.shot = clickedElement.data('shot');
        } else {
            this.element.get(0).dataset.preparation = clickedElement.data('preparation');
        }

        this.element.addClass('is-hidden');
        new MilkSelection();
        this.destroy();
    }

    showSelection(): void {
        if (this.orderType === 'custom') {
            this.element.find(`.coffee-type__list`).addClass('is-hidden');
            this.element.find('.coffee-type__custom').removeClass('is-hidden');
        }
    }

    destroy(): void {
        this.element.find(`.coffee-type__list-item`).off('click.coffee-type');
    }
}

class Size {
    private element: JQuery = $('.coffee-size');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.size__option`).on('click.coffee-size', this.chooseSize.bind(this));
    }

    chooseSize(event: JQuery.ClickEvent): void {
        const clickedElement: JQuery = $(event.currentTarget);
        const chosenSize: string = clickedElement.data('size')

        clickedElement.addClass('is-selected');
        this.element.get(0).dataset.size = chosenSize;

        this.element.addClass('is-hidden');
        new CoffeeType();
        this.destroy();
    }

    destroy(): void {
        this.element.find(`.size__option`).off('click.coffee-size');
    }
}

class ChooseCoffee {
    private element: JQuery = $('.coffee-choice');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find('.coffee-choice__classic').on('click.coffee-choice', this.chooseType.bind(this));
        this.element.find('.coffee-choice__custom').on('click.coffee-choice', this.chooseType.bind(this));
    }

    chooseType(event: JQuery.ClickEvent): void {
        const currentTarget = $(event.currentTarget);

        if (currentTarget.hasClass('coffee-choice__classic')) {
            this.element.get(0).dataset.type = 'classic';
        } else {
            this.element.get(0).dataset.type = 'custom';
        }

        new Size();
        this.element.addClass('is-hidden');
        this.destroy();
    }

    destroy(): void {
        this.element.off('click.coffee-choice');
    }
}

class Login {
    private element: JQuery = $('.login');

    private ajax: JQuery.jqXHR;

    static username: string;

    constructor() {
        this.init();
    }

    init(): void {
        this.element.removeClass('is-hidden');
        this.element.find(`.login__login`).on('click.login', this.login.bind(this));
        this.element.find(`.login__register`).on('click.login', this.register.bind(this));
    }

    destroy(): void {
        this.element.off('click.login');
    }

    login(event: JQuery.ClickEvent): void {
        event.preventDefault();

        const username: string = String(this.element.find(`.login__username`).val());
        const password: string = String(this.element.find(`.login__password`).val());

        if (this.ajax) {
            this.ajax.abort();
        }

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'login',
                username: username,
                password: password
            }
        });

        //login logic
        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.destroy();
                this.element.addClass('is-hidden');
                Login.username = username;
                new ChooseCoffee();
            } else {
                alert('Wrong username or password, please try again');
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to login, please try again later :(');
        });
    }

    register(event: JQuery.ClickEvent): void {
        event.preventDefault();

        const username: string = String(this.element.find(`.login__username`).val());
        const password: string = String(this.element.find(`.login__password`).val());

        if (this.ajax) {
            this.ajax.abort();
        }

        this.ajax = $.ajax({
            method: 'POST',
            url: Ajax.path,
            data: {
                action: 'add-user',
                username: username,
                password: password
            }
        });

        this.ajax.then((response: string) => {
            const parsedResponse: ILoginResponse = JSON.parse(response);

            if (parsedResponse.status === 'success') {
                this.destroy();
                this.element.addClass('is-hidden');
                Login.username = username;
                new ChooseCoffee();
            }
        });

        this.ajax.catch(() => {
            console.warn('Failed to register');
            alert('Failed to register, please try again later :(');
        });
    }
}

class Page {
    private element: JQuery = $('.page');

    constructor() {
        this.init();
    }

    init(): void {
        this.element.find(`.loading`).addClass('is-hidden');
        new Login();
    }
}

$(() => {
    new Page();
});
