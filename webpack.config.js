const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        engine: './src/index.ts'
    },
    watch: true,
    watchOptions: {
        ignored: '**/node_modules',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    'ts-loader',
                ],
                exclude: '/node_modules/',
            },
            {
                test: /\.s[ac]ss$/i,
                exclude: '/node_modules/',
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
                ]
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js', '.scss' ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'assets/styles.css'
        })
    ],
    output: {
        filename: 'assets/bundle.js',
        path: path.resolve('public'),
    },
}
